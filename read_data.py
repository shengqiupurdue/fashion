import json
from collections import Counter


def get_stats(data):
    total_images = len(data['images'])

    all_annotations = []
    if 'annotations' in data:
        for each in data['annotations']:
            all_annotations.extend(each['labelId'])
    total_labels = len(set(all_annotations))
    return total_images, total_labels, all_annotations


train_path = 'raw_data/train.json'
test_path = 'raw_data/test.json'
valid_path = 'raw_data/validation.json'

train_inp = open(train_path).read()
train_inp = json.loads(train_inp)
test_inp = open(test_path).read()
test_inp = json.loads(test_inp)
valid_inp = open(valid_path).read()
valid_inp = json.loads(valid_inp)


train_total_images, train_total_labels, train_annotations = get_stats(train_inp)
print('train dataset has total {} images, totally {} labels'.format(train_total_images, train_total_labels))
valid_total_images, valid_total_labels, valid_annotations = get_stats(valid_inp)
print('valid dataset has total {} images, totally {} labels'.format(valid_total_images, valid_total_labels))
test_total_images, test_total_labels, test_annotations = get_stats(test_inp)
print('test dataset has total {} images, totally {} labels'.format(test_total_images, test_total_labels))


# this function get basic stats like total number of images or total number of labels
train_labels = Counter(train_annotations)
